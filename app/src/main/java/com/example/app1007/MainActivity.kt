﻿package com.example.app1007

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
  // 宣告介面元件
    private lateinit var etChScore:EditText
    private lateinit var ivChCheck:ImageView
    private lateinit var btCheck:Button
    private lateinit var etEnScore: EditText
    private lateinit var etMathScore: EditText
    private lateinit var ivEnCheck: ImageView
    private lateinit var ivMathCheck: ImageView

    private var btCheckOnClick = View.OnClickListener {
        var chScore = etChScore.text.toString().toDouble()
        if (chScore >=60)
            ivChCheck.setImageResource(R.drawable.ic_pass)
        else
            ivChCheck.setImageResource(R.drawable.ic_fail)

        var enScore = etEnScore.text.toString().toDouble()
        if (enScore >=60)
            ivEnCheck.setImageResource(R.drawable.ic_pass)
        else
            ivEnCheck.setImageResource(R.drawable.ic_fail)

        var mathScore = etMathScore.text.toString().toDouble()
        if (enScore >=60)
            ivMathCheck.setImageResource(R.drawable.ic_pass)
        else
            ivMathCheck.setImageResource(R.drawable.ic_fail)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etChScore = findViewById(R.id.etChScore)
        ivChCheck = findViewById(R.id.ivChCheck)
        btCheck = findViewById(R.id.btCheck)
        etEnScore = findViewById(R.id.etEnScore)
        ivEnCheck = findViewById(R.id.ivEnCheck)
        etMathScore = findViewById(R.id.etMathScore)
        ivMathCheck = findViewById(R.id.ivMathCheck)

        btCheck.setOnClickListener(btCheckOnClick)
    }
}

